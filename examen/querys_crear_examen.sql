--#############################--
--##### QUERY PRACTICANTE #####--
--#############################--

------------------------------------------------------------

/* QUERY PARA ARMAR LAS TABLAS NECESARIAS DESDE EL SERVIDOR DE QROMA */

------------------------------------------------------------

USE CAA_MODELOS;

DROP TABLE CAA_P_TABLA1;

-- TABLA 1: TABLA_VENTAS

SELECT
NU_DOCU,
TI_DOCU,
ID_ITEM,
ID_CLIE,
FE_DOCU,
IM_VENT_NACI,
IM_DESC_NACI,
IM_COST_MNAC,
CA_DOCU,
ST_PROM
INTO CAA_P_TABLA1
FROM [CPPQ5].[DATAMART].[dbo].[TMFACT_VENT]
WHERE CO_EMPR = '01' -- PERU 
AND YEAR(FE_DOCU) IN ('2018')
AND ID_TIPO_FACT IN ('04','05'); --FEP, FER

------------------------------------------------------------

DROP TABLE CAA_P_TABLA2;

-- TABLA 2: TABLA_ITEMS

SELECT
ID_ITEM,
REPLACE(DE_ITEM,',','') DE_ITEM,
REPLACE(DE_CATE,',','') DE_CATE,
REPLACE(DE_EQUI,',','') DE_EQUI,
REPLACE(DE_FAMI,',','') DE_FAMI
INTO CAA_P_TABLA2
FROM [CPPQ5].[DATAMART].[dbo].[TMDIME_ITEM] 
WHERE TI_ITEM = 'PTE' -- Productos Terminados 
AND CO_EMPR = '01';

------------------------------------------------------------

DROP TABLE CAA_P_TABLA3;

-- TABLA 3: TABLA_CLIENTE

SELECT
ID_CLIE,
REPLACE(NO_CLIE,',','') NO_CLIE,
DE_TIPO_CLIE,
TI_SOCI
INTO CAA_P_TABLA3
FROM [CPPQ5].[DATAMART].[dbo].[TMDIME_CLIE]
WHERE CO_TIPO_CLIE IN ('FER','FEP');

------------------------------------------------------------

/* QUERY PARA CREAR Y SUBIR A LA VIRTUAL MACHINE*/

------------------------------------------------------------

/*

Desde la VM entrar a SQL: 

sqlcmd -S localhost -U SA -P "Practicante 1" 

*/

------------------------------------------------------------

-- TABLA 1: TABLA VENTAS

-- Crear tabla 
CREATE TABLE TABLA_VENTAS (NU_DOCU varchar(20) NOT NULL, TI_DOCU varchar(3), ID_ITEM varchar(20), ID_CLIE varchar(20), FE_DOCU datetime, IM_VENT_NACI float, IM_DESC_NACI float, IM_COST_MNAC float, CA_DOCU float, ST_PROM varchar(2));

-- Subir data
BULK INSERT TABLA_VENTAS FROM '/home/arojas150294_gmail_com/Tabla1_Final.csv' WITH ( FIRSTROW = 2,  FIELDTERMINATOR = ',',  ROWTERMINATOR = '\n', TABLOCK);

-- Verificar 
SELECT TOP 10 * FROM TABLA_VENTAS;

------------------------------------------------------------

-- TABLA 2: TABLA ITEMS

-- Crear tabla
CREATE TABLE TABLA_ITEMS (ID_ITEM varchar(20) NOT NULL, DE_ITEM varchar(225), DE_CATE varchar(225), DE_EQUI varchar(225), DE_FAMI varchar(225));

-- Subir data 
BULK INSERT TABLA_ITEMS FROM '/home/arojas150294_gmail_com/Tabla2_Final.csv' WITH ( FIRSTROW = 2,  FIELDTERMINATOR = ',',  ROWTERMINATOR = '\n', TABLOCK);

-- Verificar
SELECT TOP 10 * FROM TABLA_ITEMS;

------------------------------------------------------------

-- TABLA 3: TABLA CLIENTE

-- Crear tabla
CREATE TABLE TABLA_CLIENTE (ID_CLIE varchar(20) NOT NULL, NO_CLIE varchar(225), DE_TIPO_CLIE varchar(225), TI_SOCI varchar(225));

-- Subir data
BULK INSERT TABLA_CLIENTE FROM '/home/arojas150294_gmail_com/Tabla3_Final.csv' WITH ( FIRSTROW = 2,  FIELDTERMINATOR = ',',  ROWTERMINATOR = '\n', TABLOCK);

-- Verificar
SELECT TOP 10 * FROM TABLA_CLIENTE;

------------------------------------------------------------

/* QUERY PARA OBTENER EJEMPLO DE CANTIDADES */

SELECT ID_CLIE, SUM(IM_VENT_NACI-IM_DESC_NACI) AS VENTA_TOT_REAL,
SUM(IM_VENT_NACI-IM_DESC_NACI - IM_COST_MNAC) AS UTILIDAD_TOT
FROM TABLA_VENTAS WHERE ID_CLIE = '175628685'
GROUP BY ID_CLIE;

------------------------------------------------------------