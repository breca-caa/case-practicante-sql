-------------------------------------------------------------------------------

/* RESULTADOS EVALUACION PRACTICANTE CAA - SQL */

-- Diego 

P1
SELECT MONTH(fe_docu) AS mes, SUM(im_vent_naci - im_desc_naci) AS venta_total, SUM(im_vent_naci - im_desc_naci - im_cost_mnac) AS utilidad_total
FROM tabla_ventas WHERE MONTH(fe_docu) IN (1,2,3) AND YEAR(fe_docu) = 2018 GROUP BY MONTH(fe_docu) ORDER BY MONTH(fe_docu); 
-- 5 puntos

P2
SELECT TOP 10 c.no_clie AS nombre_clie, SUM(v.im_vent_naci -v.im_desc_naci) AS venta_total
FROM tabla_ventas AS v INNER JOIN tabla_cliente AS c ON v.id_clie=c.id_clie
GROUP BY c.no_clie ORDER BY venta_total DESC;
-- 5 puntos

P4
SELECT c.ti_soci AS tipo_clie, COUNT(v.id_clie) AS num_clie, SUM(v.im_vent_naci-v.im_desc_naci) AS venta_total, 
COUNT(v.id_item) AS num_item FROM tabla_cliente AS c INNER JOIN tabla_ventas AS v ON c.id_clie = v.id_clie
WHERE v.id_clie IS NOT NULL AND v.id_item IS NOT NULL GROUP BY c.ti_soci;
-- 4 puntos (los count deberian ser count distinct)

-- Total: 14

-------------------------------------------------------------------------------

-- Andres Regal

P1
SELECT ID_CLIE, SUM(IM_VENT_NACI - IM_DESC_NACI) AS VENTA_TOT_REAL, SUM(IM_VENT_NACI - IM_DESC_NACI - IM_COST_MNAC) AS UTILIDAD_TOT FROM TABLA_VENTAS WHERE WHERE FE_DOCU BETWEEN '2018-02-01 00:00:00' and '2018-04-01 00:00:00' GROUP BY ID_CLIE;
-- 3 puntos (tenia que agrupar por mes, era un cuadro resumen)

P2
SELECT TOP 10 NO_CLIE, VENTA_TOT_REAL FROM (SELECT ID_CLIE, SUM(IM_VENT_NACI) AS VENTA_TOT_REAL, NO_CLIE FROM TABLA_VENTAS INNER JOIN TABLA_CLIENTE ON TABLA_VENTAS.ID_CLIE = TABLA_CLIENTE.ID_CLIE GROUP BY ID_CLIE) as AAA;
-- 4 puntos (faltó la resta correcta para ventas y agrupar correctamente)

-- Total: 7

-------------------------------------------------------------------------------

-- Carlos Huerta

select SUM(IM_VENT_NACI) AS INGRESO_TOTAL,SUM(IM_VENT_NACI-IM_DESC_NACI-IM_COST_MNAC) AS UTILIDAD_TOTAL FROM TABLA_VENTAS where year(FE_DOCU) = 2018 AND month(FE_DOCU)>= DATE_SUB(month(FE_DOCU)=01,INTERVAL 3 month) and month(FE_DOCU) <= 03;

-- 3 puntos (no agrupo por mes y tampoco tiene la resta correcta)

-- Total: 3

-------------------------------------------------------------------------------

-- Gherald Barzola

Pregunta 1)

select sum((IM_VENT_NACI)*CA_DOCU) AS INGRESO_TOTAL,
       sum((IM_VENT_NACI-IM_DESC_NACI-IM_COST_MNAC)*CA_DOCU) AS UTILIDAD_TOTAL,
       datename(month,FE_DOCU) AS MES
from   TABLA_VENTAS
where FE_DOCU between '2018-01-01' and '2018-03-31'
group by datename(month,FE_DOCU)

GO

-- 3 puntos (no calculo correctamente las ventas totales, la indicacion era que no use ca_docu claramente)

Pregunta 2)

select TOP(10)NO_CLIE,
	VENTA_TOT_REAL
FROM
(select  A.ID_CLIE,
	B.NO_CLIE,
	SUM(((A.IM_VENT_NACI)-(A.IM_DESC_NACI))*(A.CA_DOCU)) AS VENTA_TOT_REAL
from TABLA_VENTAS A
left join TABLA_CLIENTE B
on A.ID_CLIE = B.ID_CLIE) X
order by VENTA_TOT_REAL DESC
GO

-- 3 puntos (no calculo la venta correctamente, no agrupo tampoco)

Pregunta 3)

select TOP(1)Z.DE_FAMI,
       (Z-VENTA_TOT_REAL_MAR-Z.VENTA_TOT_REAL_FEB)/Z.VENTA_TOT_REAL_FEB AS PERCENT_FEB_MAR
(select A.DE_FAMI,
       SUM(CASE WHEN datename(month,B.FE_DOCU) = 'February' THEN ((B.IM_VENT_NACI)-(B.IM_DESC_NACI))*(B.CA_DOCU) ELSE 0 END) AS VENTA_TOT_REAL_FEB,
       SUM(CASE WHEN datename(month,B.FE_DOCU) = 'March' THEN ((B.IM_VENT_NACI)-(B.IM_DESC_NACI))*(B.CA_DOCU) ELSE 0 END) AS VENTA_TOT_REAL_MAR
from TABLA_ITEMS A
left join TABLA_VENTAS B
on A.ID_ITEM = B.ID_ITEM
where B.FE_DOCU between '2018-02-01' and '2018-03-31'
group by A.DE_FAMI) Z
order by (Z-VENTA_TOT_REAL_MAR-Z.VENTA_TOT_REAL_FEB)/Z.VENTA_TOT_REAL_FEB
GO

--4 puntos (no calcula la venta correctamente)

Pregunta 4)

select 
from 

-- Total 10 

-------------------------------------------------------------------------------

-- Antonio Ordoñez

1. select MONTH(FE_DOCU) as MES, SUM(IM_VENT_NACI - IM_DESC_NACI) as VENTA_TOT_REAL
FROM TABLA_VENTAS
WHERE MONTH(FE_DOCU) < 4
GROUP BY month(FE_DOCU) ORDER BY MES;
-- 4 puntos (falto utilidad)

2.
SELECT TOP 10 TABLA_VENTAS.ID_CLIE, TABLA_CLIENTE.NO_CLIE, SUM(IM_VENT_NACI - IM_DESC_NACI) AS TOTAL_CLIENTE
FROM TABLA_VENTAS
INNER JOIN TABLA_CLIENTE ON TABLA_VENTAS.ID_CLIE = TABLA_CLIENTE.ID_CLIE 
GROUP BY TABLA_CLIENTE.ID_CLIE ORDER BY TOTAL_CLIENTE;
-- 4 puntos (falto agrupar por nombre cliente para que corra)


3. 
SELECT  ID_ITEM, MONTH(FE_DOCU), SUM(IM_VENT_NACI - IM_DESC_NACI) AS VENTAMARZO 
FROM  TABLA_VENTAS  WHERE MONTH(FE_DOCU) = 3 GROUP BY month(FE_DOCU), ID_ITEM;

SELECT  ID_ITEM, MONTH(FE_DOCU), SUM(IM_VENT_NACI - IM_DESC_NACI) AS VENTAMARZO 
FROM  TABLA_VENTAS  WHERE MONTH(FE_DOCU) = 4 GROUP BY month(FE_DOCU), ID_ITEM;
-- 1 punto, tenia que empezar agrupando por familia para luego hacer los siguientes pasos


4.  SELECT COUNT(TABLA_VENTAS.ID_CLIE), TI_SOCI FROM TABLA_VENTAS INNER JOIN TABLA_CLIENTE ON TABLA_VENTAS.ID_CLIE = TABLA_CLIENTE.ID_CLIE GROUP BY TI_SOCI;
-- Era count distinct, falto el count distinct de items y el sum de ventas
-- 2 puntos 

-- Total: 11 

-------------------------------------------------------------------------------

-- Victoria Zeballos 

SQL

1> select top 3 Month(fe_docu), sum(IM_VENT_NAcI), sum(IM_VENT_NACI-IM_DESC_NACI-IM_COST_MNAC) from TABLA_VENTAS
2> WHERE MONTH(FE_DOCU)<=3
3> GROUP BY month(fe_docu)
4> order by month(fe_docu) asc
5> go

-- 4 puntos (no hizo la resta correcta)


1>select top 10 no_clie, sum(a.IM_VENT_NAcI), sum(a.IM_VENT_NACI-a.IM_DESC_NACI-a.IM_COST_MNAC) from TABLA_VENTAS a join tabla_cliente b on a.id_clie=b.id_clie
2>group by b.no_clie
3>order by sum(a.im_vent_naci) desc
4>go

-- 4 puntos (no hizo la resta correcta)

1>select ti_socio, count(a.id_clie), count(distict(id_items)) a.IM_VENT_NACI-a.IM_DESC_NACI-a.IM_COST_MNAC) from TABLA_VENTAS a join tabla_cliente b on a.id_clie=b.id_clie join tabla_items on a.id_item=c.id_item
2>group by ti_socio
3>order by sum(a.im_vent_naci) desc
4>go

-- 2 puntos (no hizo el count distinct en clientes), falto sumar lo que se le pedia, no obtuvo lo que se le pedia 

-------------------------------------------------------------------------------

-- Rubi Ore

PREGUNTA 1:
SELECT SUM(IM_VENT_NACI)- SUM(IM_DESC_NACI)AS INGRESO_TOTAL,SUM(IM_VENT_NACI)-SUM(IM_DESC_NACI)- SUM(IM_COST_MNAC)AS UTILIDAD_TOTAL FROM TABLA_VENTAS WHERE MONTH(FE_DOCU) IN ('1','2','3') GROUP BY MONTH(FE_DOCU);

-- 5 puntos 

PREGUNTA 2:
SELECT TOP 10 C.NO_CLIE, (SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI))AS MONTO FROM TABLA_VENTAS AS V INNER JOIN TABLA_CLIENTE AS C ON V.ID_CLIE=C.ID_CLIE GROUP BY C.NO_CLIE ORDER BY (SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI)) ASC;

-- 5 puntos 

PREGUNTA 3:
SELECT TOP 1 I.DE_FAMI FROM TABLA_VENTAS AS V INNER JOIN TABLA_ITEMS AS I ON V.ID_ITEM=I.ID_ITEM  ORDER BY (SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI) AND MONTH(V.FE_DOCU)='3')/(SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI) AND MONTH(V.FE_DOCU)='2') DESC

-- 2 puntos 

PREGUNTA 4:
SELECT C.TI_SOCI, COUNT(*) AS CANT_CLIENTES, SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI)AS COMPRA_TOTAL, COUNT (DISTINCT I.ID_ITEM) AS NRO_ITEMS FROM TABLA_ITEMS AS I INNER JOIN TABLA_VENTAS AS V ON I.ID_ITEM=V.ID_ITEM INNER JOIN TABLA_CLIENTE AS C ON V.ID_CLIE=C.ID_CLIE GROUP BY C.TI_SOCI

-- 4 puntos (count distinct en cantidad de clientes)

PREGUNTA 5:
SELECT TOP 1 ..SUM(V.IM_VENT_NACI)- SUM(V.IM_DESC_NACI) I.DE_EQUI;

-- Total: 16 

-------------------------------------------------------------------------------

-- Jose Chu

Pregunta 1

select sum(im_vent_naci-im_desc_naci) VENTAS_REALES, sum(im_vent_naci - im_desc_naci - im_cost_mnac) UT from tabla_ventas 

-- 3 puntos, no agrupo por mes, ni filtro los meses necesarios

Pregunta 2

select distinct top 10 tc.no_clie, ventas.vr from tabla_cliente as tc left join (select id_clie, nu_docu, sum(im_vent_naci-im_desc_naci )vr from tabla_ventas where datepart

-- 4 puntos (falta el on y el filtro?, pero tiene la query el output bien, probable se equivoco al copiarla)

Pregunta 3

 select id_item, sum(im_vent_naci-im_desc_naci) vr into #ventas_feb
 from tabla_ventas where datepart(yyyy,fe_docu) = 2018 and datepart(mm, fe_docu) = 02 group by id_item

 select id_item, sum(im_vent_naci-im_desc_naci) vr into #ventas_mar
 from tabla_ventas where datepart(yyyy,fe_docu) = 2018 and datepart(mm, fe_docu) = 03 group by id_item

select top 1 ti.de_fami, (vm.vr - vf.vr) dacaida from #ventas_feb
as vf,
#ventas_mar as vm,
tabla_items ti where vr.id_item=vf.id_item and ti.id_item = vr.id_item;

-- 4.5 puntos (solo falto dividir para obtneer la caida porcentual y no absoluta)

-------------------------------------------------------------------------------
